window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();

var AutoHover = function(_type) {
  this._type = _type;
  this._targets = document.getElementsByClassName(this._type);
  if (!this._targets.length)	return;
  this.init();
};

AutoHover.prototype = {
  init : function() {
    var _self = this;
    var i = 0|0;
    for (i = 0; i<this._targets.length; i=(i+1)|0) {
      this._targets[i].addEventListener('mouseenter', function(e) {_self.onMouseOver({currentTarget:e.currentTarget, self:_self})} , false);
      this._targets[i].addEventListener('mouseleave', function(e) {_self.onMouseOut({currentTarget:e.currentTarget, self:_self})} , false);
    };
  },
  onMouseOver:function(e) {
    if (e.self._type === 'img_ovr') {
      e.currentTarget.setAttribute('src', e.currentTarget.getAttribute('src').replace(/_off/ig, '_on'));
      return;
    }
    Velocity(e.currentTarget, 'stop');
    Velocity(e.currentTarget, {opacity:0.7}, {duration:300, delay:0, easing:'easeOutSine'});
  },
  onMouseOut:function(e) {
    if (e.self._type === 'img_ovr') {
      e.currentTarget.setAttribute('src', e.currentTarget.getAttribute('src').replace(/_on/ig, '_off'));
      return;
    }
    Velocity(e.currentTarget, 'stop');
    Velocity(e.currentTarget, {opacity:1}, {duration:300, delay:0, easing:'easeInSine'});
  }
}

var PageTop = function() {
  this._targets = document.getElementsByClassName('pagetop');
  if (!this._targets.length)	return;
  this.init();
}
PageTop.prototype = {
  _isShow : false,
  init : function() {
    var _self = this;
    window.addEventListener('scroll', _.debounce(function() {_self.onScroll();}, 50) , false);
  },
  onScroll : function() {
    var _top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
    _top > 0 ? this.show() : this.hide();
  },
  show : function() {
    if (this._isShow)	return;
    this._isShow = true;
    this.setVisible(1);
  },
  hide : function() {
    if (!this._isShow)	return;
    this._isShow = false;
    this.setVisible(0);
  },
  setVisible : function(_opacity) {
    var i = 0|0;
    for (i = 0; i<this._targets.length; i=(i+1)|0) {
      Velocity(this._targets[i], 'stop');
      Velocity(this._targets[i], {opacity:_opacity}, {duration:300, delay:0, easing:'easeOutSine'});
    };
  }
}

var AnchorLink = function() {
  this._targets = document.querySelectorAll('a[href^="#"]');
  if (!this._targets.length)	return;
  this.init();
}
AnchorLink.prototype = {
  init : function() {
    var i = 0|0;
    for (i = 0; i<this._targets.length; i=(i+1)|0) {
      this._targets[i].addEventListener('click', this.onClickHD , false);
    };
  },
  onClickHD : function(e) {
    var _hash = e.currentTarget.getAttribute('href').replace('#', '');
    Velocity(document.getElementById(_hash), 'scroll', {duration:1000, delay:0, easing:'easeInOutSine'});
    e.preventDefault();
  }
}

var Base = function() {
  !function Base() {
    new AutoHover('img_ovr');
    new AutoHover('alpha_ovr');
    new PageTop();
    new AnchorLink();
  }();
  return Base;
};

window.addEventListener('DOMContentLoaded', function() {
  if (window.jQuery) window.Velocity = window.jQuery.fn.velocity;
  new Base();
});

/* images pc <---> sp */
(function () {
  var PicturePolyfill = (function () {
    function PicturePolyfill() {
      var _this = this;
      this.pictures = [];
      this.onResize = function () {
        var width = document.body.clientWidth;
        for (var i = 0; i < _this.pictures.length; i = (i + 1)) {
          _this.pictures[i].update(width);
        };
      };
      if ([].includes) return;
      var picture = Array.prototype.slice.call(document.getElementsByTagName('picture'));
      for (var i = 0; i < picture.length; i = (i + 1)) {
        this.pictures.push(new Picture(picture[i]));
      };
      window.addEventListener("resize", this.onResize, false);
      this.onResize();
    }
    return PicturePolyfill;
  }());
  var Picture = (function () {
    function Picture(node) {
      var _this = this;
      this.update = function (width) {
        width <= _this.breakPoint ? _this.toSP() : _this.toPC();
      };
      this.toSP = function () {
        if (_this.isSP) return;
        _this.isSP = true;
        _this.changeSrc();
      };
      this.toPC = function () {
        if (!_this.isSP) return;
        _this.isSP = false;
        _this.changeSrc();
      };
      this.changeSrc = function () {
        var toSrc = _this.isSP ? _this.srcSP : _this.srcPC;
        _this.img.setAttribute('src', toSrc);
      };
      this.img = node.getElementsByTagName('img')[0];
      this.srcPC = this.img.getAttribute('src');
      var source = node.getElementsByTagName('source')[0];
      this.srcSP = source.getAttribute('srcset');
      this.breakPoint = Number(source.getAttribute('media').match(/(\d+)px/)[1]);
      this.isSP = !document.body.clientWidth <= this.breakPoint;
      this.update();
    }
    return Picture;
  }());
  new PicturePolyfill();
}());
